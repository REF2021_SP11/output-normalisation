# Output Normalisation

*Output Normalisation.py* is the Python code used by REF 2021 SP11 to correct for differences in the Output scoring behaviour of reviewers. Details of the methods used can be found in the Working Methods document in the Documents project.

We hope to be able to provide the real REF 2021 data in anonymised form to allow testing and experimentation, but that has not yet been agreed by the REF Team. If it is not agreed, we will consider generating comparable synthetic data.
