# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 10:58:41 2020

@author: Chris Taylor
"""

###############################################################################
''' 
Program to normalise and combine scores for set of outputs each scored by 3
reviewers (hardwired but easily generalisable)

Inputs:
    Spreadsheet with two worksheets
       Score Data: Output ID, Panellist 1-3, Score P1-3
       Parameters: Iterations, Spotlight (reviewer)
Outputs:
    Spreadsheet with eight worksheets
        Output Scores: as input plus Normalised score P1-3, ML score, % Prob
        Calibration Table: reviewer x raw score table of normalised scores
        Reviewer Variance: table of estimated variance by reviewer
        Redist Tholds: table of redistribution thresholds
        Reviewer Pairing: table of paired reviewer counts
        Spotlight Actuals: score histogram for spotlight reviewer by iteration
        Spotlight Targets: target histogram for spotlight reviewer by iteration
        Run Summary: summary of parameters, input file and software version
        
Notes:
    The approach involves iteratively computing a reshaping function for
    each reviewer's score distribution, such that their reshaped distribution
    is identical to the distribution obtained by other reviewers
    on the same set of outputs. The overall distibution of scores for all
    reviewers over all outputs is held constant. The ML score is the maximum 
    likelihood combination of the three normalised scores for each output, 
    computed using the estimated reviewer variances. See the published SP11
    Working Methods document for more detail.
'''
###############################################################################

import tkinter.filedialog
import numpy as np
import math as mt
import pandas as pd
import random as rand
import itertools as it
from datetime import datetime as dt

# Select an Excel file and read worksheet into pandas DataFrame
###############################################################
root = tkinter.Tk()  # define the root Tk window
filename = tkinter.filedialog.askopenfilename(parent=root)  # browse widget in root
excel_file = pd.ExcelFile(filename)
fields = ['Output Identifier', 'Panellist 1', 'Panellist 2', 'Panellist 3', 
          'P1 Score', 'P2 Score', 'P3 Score']
scores = pd.read_excel(excel_file, sheet_name='Score Data', 
                       index_col='Output Identifier', usecols=fields)
parameters = pd.read_excel(excel_file, sheet_name='Parameters', index_col='Index')
root.destroy()  # destroy the root Tk window

# Define keys for output ID, reviewers, score and normalised score columns, and scores
######################################################################################
# Extract output ID keys
op_keys = scores.index.tolist()
# Define keys for reviewer, score and normalised score columns
rev_cols = ['Panellist 1', 'Panellist 2', 'Panellist 3']
score_cols = ['P1 Score', 'P2 Score', 'P3 Score']
nscore_cols = ['P1 Nscore', 'P2 Nscore', 'P3 Nscore']
# Extract Reviewer keys
rev_keys = sorted(list(set(pd.concat([scores[i] for i in rev_cols]).tolist())))
# Extract range of scores and create keys (NB max, min don't care about NaN values) 
score_list = pd.concat([scores[i] for i in score_cols])
score_keys = [i for i in range(int(score_list.min()), int(score_list.max() + 1))]

#For each ra, compute score distribution (profile) for co-reviewed outputs for each rb  
######################################################################################
# Create a list of empty reviewer x scores DataFrames, one for each reviewer
rawpair_dfs = [pd.DataFrame(index=rev_keys, columns=score_keys, data=0) for k in rev_keys]
# Create a dict of the DataFrames, indexed by reviewer
rawpair_profs = dict(zip(rev_keys, rawpair_dfs))
# Cycle through the outputs to populate distributions
for op in op_keys:
    # Create list of reviewers for this output
    revs = scores.loc[op, rev_cols].tolist()
    # Create list of scores for this output
    rscores = scores.loc[op, score_cols].tolist()
    # Create dict of scores indexed by reviewer
    rscore_dict = dict(zip(revs, rscores))
    # Remove reviewers with NaN scores from list of reviewers
    revs = [k for k, v in rscore_dict.items() if not mt.isnan(v)]
    # Create list of reviewer pairs for this output
    pairs = list(it.product(revs, revs))
    # For each pair (ra, rb) increment element (rb, score[rb]) in ra pair profile
    for p in pairs: rawpair_profs[p[0]].loc[p[1], rscore_dict[p[1]]] +=1
    
# Extract raw cumulative profile and own/total reviews for each reviewer
########################################################################
# Create empty rev_keys x score_keys DataFrame
raw_profs = pd.DataFrame(index=rev_keys, columns=score_keys)
# Populate df for each reviewer with cumulative sum of their row in their pair profile
for r in rev_keys: raw_profs.loc[r] = rawpair_profs[r].loc[r].cumsum()
# Add final zeroed column with column label (min score - 1)
raw_profs[score_keys[0] - 1] = 0
# For each reviewer, compute (total own reviews) / (total reviews for same outputs)
rats = [rawpair_profs[i].loc[i].sum() / rawpair_profs[i].sum().sum() for i in rev_keys]
# Replace NaN values with 0 to trap reviewers with no shared reviews
rats = [0 if np.isnan(v) else v for v in rats]
# Create dict of ratios indexed by reviewer 
rev_rats = dict(zip(rev_keys, rats))

# Initialise redistribution tables, redistribution thresholds and target profiles
#################################################################################
# Create zeroed scores x scores redistribution table, data type float
df_init = pd.DataFrame(index=score_keys, columns=score_keys, data=0, dtype=float)
# Set diagonal elements to 1 to initialise
for i in score_keys: df_init.loc[i, i] = 1
# Create dict of initialised redistribution tables, indexed by reviewer
redist_tabs = dict(zip(rev_keys, [df_init.copy() for r in rev_keys]))
# Create empty reviewers x scores DataFrame for target profiles
targ_profs = pd.DataFrame(index=rev_keys, columns=score_keys, dtype=float)
# Create zeroed reviewers x scores DataFrame for redistribution thresholds
redist_tholds = pd.DataFrame(index=rev_keys, columns=score_keys, dtype=float, data=0)
# Add final column with label (min score - 1) and values -1 (for score-1 access)
redist_tholds[score_keys[0] - 1] = -1

# Read input parameters and create DataFrames for spotlight data
################################################################
# Spotlight reviewer for generating convergence illustration
spot_rev = parameters.loc['Spotlight', 'Value']
# Number of iterations
iterations = parameters.loc['Iterations', 'Value']
# Create DataFrame for actual and target distributions for spotlight reviewer
actual_iters = pd.DataFrame(index=range(iterations), columns=score_keys)
target_iters = pd.DataFrame(index=range(iterations), columns=score_keys)

#####################
# Main iterative loop
#####################
for iter in range(iterations):
    for ra in rev_keys:
        
        # Compute target_profile for ra
        ###############################
        # Reshape all pair profiles for ra using (redist_tabs[rb])•(rawpair_profs[rb])  
        prof_list = [redist_tabs[rb].dot(rawpair_profs[ra].loc[rb])for rb in rev_keys]
        # Sum profiles, weighted by own/total ratio; compute cumulative target profile 
        targ_profs.loc[ra] = sum([pl * rev_rats[ra] for pl in prof_list]).cumsum()
        
        # If ra is the spotlight reviewer, save actual and target distributions
        #########################################################################
        if ra == spot_rev:
            actual_iters.loc[iter] = redist_tabs[ra].dot(rawpair_profs[ra].loc[ra])
            target_iters.loc[iter] = sum([pl * rev_rats[ra] for pl in prof_list])
        
        # Compute redistribution thresholds for ra
        ##########################################
        # Find threshold for each score value (NB raw_profs defined for all score - 1) 
        for s in score_keys:
            # Set target value to target profile [ra, s] unless > total reviews
            tval = min(targ_profs.loc[ra, s], raw_profs.loc[ra, score_keys[-1]])
            # Find score ge_score for which raw profile for ra is first >= tval
            ge_score = raw_profs.loc[ra][raw_profs.loc[ra].ge(tval)].index[0]
            # Find difference between target and raw profile at ge_score - 1
            top = float(targ_profs.loc[ra, s] - raw_profs.loc[ra, ge_score - 1])
            # Find difference between raw profile at ge_score and ge_score - 1
            bot = float(raw_profs.loc[ra, ge_score] - raw_profs.loc[ra, ge_score - 1])
            # Interpolate redistribution threshold between ge_score - 1 and ge_score
            try:
                redist_tholds.loc[ra, s] = (ge_score - 1) + top / bot
            # Unless error when threshold is ge_score - 1
            except Exception:
                redist_tholds.loc[ra, s] = ge_score - 1
    
    # Re-compute redistribution tables
    ##################################
    for ra in rev_keys:
        # Loop over rows in the redistribtion table for ra
        for s in score_keys:
            # Find the bracketing thresholds for score s 
            lo = redist_tholds.loc[ra, s - 1]
            hi = redist_tholds.loc[ra, s]
            # Loop over columns in the redistribution table  
            for col in score_keys:
                # If column overlaps theshold bracket, compute table entry
                if (col >= mt.ceil(lo)) and (col <= mt.ceil(hi)):
                    redist_tabs[ra].loc[s, col] = min(hi, col) - max(lo, col - 1)
                # Otherwise set table entry to zero
                else:
                    redist_tabs[ra].loc[s, col] = 0
            
# Compute reviewer calibration table
####################################
# Create empty reviewers x scores DataFrame
rev_calib = pd.DataFrame(index=rev_keys, columns=score_keys)
# Reviewer normalised scores are dot product of score keys and redistribution table
for r in rev_keys:
    rev_calib.loc[r] = pd.Series(score_keys).dot(redist_tabs[r])

# Add normalised scores to score table, and compute n(i, j) and s(i, j)
#######################################################################
# Create zeroed DataFrames for n(i, j) and s(i, j) and add Nscore columns
n_pairs = pd.DataFrame(index=rev_keys, columns=rev_keys, data=0)     
s_pairs = pd.DataFrame(index=rev_keys, columns=rev_keys, data=0)
# Create normalised score columns
for c in nscore_cols: scores[c] = 0     
#Loop over outputs          
for i in op_keys:
    # Extract list of reviewers for this output
    revs = scores.loc[i, rev_cols].tolist()
    # Extract list of scores for this output
    rscores = scores.loc[i, score_cols].tolist()
    # Create a reviewers-scores dict
    rscore_dict = dict(zip(revs, rscores))  
    # Create reviewers-normalised score dict initialised with NaN
    nscore_dict = {r: float('NaN') for r in revs}
    # Remove reviewers with NaN scores from list of reviewers
    revs = [k for k, v in rscore_dict.items() if not mt.isnan(v)] 
    # Look up normalised scores in calibration table where score is not NaN
    for r in revs: nscore_dict[r] = rev_calib.loc[r, rscore_dict[r]]
    # Record normalised scores in scores table
    scores.loc[i,nscore_cols] = list(nscore_dict.values())
    # Save normalised score dict to scores table
    scores.loc[i, 'ns_dict'] = [nscore_dict]
    # Form list of all reviewer pairs with scores for this output
    pairs = list(it.product(revs, revs))
    # For each pair increment n(i, j) and add (nscore difference)**2 to s(i, j)
    for p in pairs: 
        n_pairs.loc[p[0], p[1]] += 1
        s_pairs.loc[p[0], p[1]] += (nscore_dict[p[0]] - nscore_dict[p[1]]) ** 2
     
# Construct arrays representing the linear equations
####################################################        
# Form list of all reviewer pairs
all_pairs = list(it.product(rev_keys, rev_keys))
# Remove self pairings from the list
all_pairs = [p for p in all_pairs if p[0] != p[1]]
# Create a zeroed numpy array for matrix m
m_array = np.zeros((len(all_pairs), len(rev_keys)))
#Create a zeroed numpy vector for vector s
s_vect = np.zeros(len(all_pairs))
# Loop over all reviewer pairs
for p in all_pairs:
    # Find i, the list index for the current reveiwer pair p
    i = all_pairs.index(p)
    # Set the vector element i to s(p[0], p[1])
    s_vect[i] = s_pairs.loc[p[0], p[1]]
    # Set the matrix elements (i,p[0]) and (i, p[1]) to n(p[0], p[1]) 
    m_array[i, rev_keys.index(p[0])] = n_pairs.loc[p[0], p[1]]
    m_array[i, rev_keys.index(p[1])] = n_pairs.loc[p[0], p[1]]
    
# Solve the linear equation mv = s; each s(i, j) = n(i,j)(var(i) + var(j))
##########################################################################
# Solve for var (also contains other information)
var = np.linalg.lstsq(m_array, s_vect, rcond=None)
# Extract the solution and create a series indexed by reviewer
var_tab = pd.Series(var[0], index=rev_keys)
    
# Insert Maximum Likelihood score for each output
#################################################
#Loop over outputs
for i in op_keys:
    # Get saved normalised score dict and remove entries with NaN values
    ns_dict = {k: v for k, v in scores.loc[i, 'ns_dict'].items() if not mt.isnan(v)}
    # Find the set of reciprocal variances for the set of reviewers with scores
    recip_vars = [1 / var_tab[r] for r in ns_dict]
    # ML score = (normalised scores)•(reciprocal variances)/sum(reciprocal variances)
    ml_score = np.dot(list(ns_dict.values()), recip_vars)/sum(recip_vars)
    # Add maximum likelihood score to score table
    scores.loc[i, 'ML Score'] = ml_score
    # Compute z-score/sqrt(2) for each of the normalised scores
    z = abs(list(ns_dict.values())-ml_score)*np.sqrt(recip_vars)/np.sqrt(2)
    # Find probability nscores differ from ml by chance and add product to score table
    scores.loc[i, '% Prob'] = 100 * np.prod([mt.erfc(z[i]) for i in range(len(ns_dict))])
        
#Output the results to a spreadsheet
####################################
# Create index for run summary Series
sum_index = ['Input File', 'Version']
# Create data for run summary Series
sum_data = [filename, 'v1.5']
# Create the indexed Series with run summary
summary = pd.Series(data=sum_data, index=sum_index)
# Append the parameters; squeeze turns single column DataFrame to Series
summary = summary.append(parameters.squeeze())  
# Construct output filename including date and time
now = dt.now()
part = filename.rpartition('/')  # extract the path by splitting at rightmost /
fileout = part[0] + '/Normalisation Result ' + now.strftime("%Y-%m-%d(%H%M)") + '.xlsx'
# Create DataFrame for reviewer variance solution output
var_soln = pd.DataFrame(all_pairs, columns=['Rev 1', 'Rev 2'])
var_soln['s_vect'] = s_vect
var_soln['s_lsq'] = m_array @ var[0]
# Output to spreadsheet
del scores['ns_dict'] # remove working column from scores dataframe
with pd.ExcelWriter(fileout) as writer:
    scores.to_excel(writer, sheet_name='Output Scores')
    rev_calib.to_excel(writer, sheet_name='Calibration Table')
    var_tab.to_excel(writer, sheet_name='Reviewer Variance')
    redist_tholds.to_excel(writer, sheet_name='Redist Tholds') # For diagnostics
    n_pairs.to_excel(writer, sheet_name='Reviewer Pairing')
    var_soln.to_excel(writer, sheet_name='var_soln')
    actual_iters.to_excel(writer, sheet_name='Spotlight Actuals')
    target_iters.to_excel(writer, sheet_name='Spotlight Targets')
    redist_tabs[spot_rev].to_excel(writer, sheet_name='Spotlight Redistribution')
    summary.to_excel(writer, sheet_name='Run Summary')
